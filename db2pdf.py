from fpdf import FPDF
import os
import pymysql
from datetime import datetime

os.chdir('Documents/aaroon/Python Assignments/Asgnt_4_6_twitterdb2pdf')
title = "Tweets from database to PDF"
conn = pymysql.connect(
    host='localhost',
    user='root',
    password='root',
    db='twitterdb',
    cursorclass=pymysql.cursors.DictCursor
)
cur = conn.cursor()


class PDF(FPDF):
    def header(self):
        self.set_font('Arial', 'B', 15)
        w = self.get_string_width(title) + 6
        self.set_x((210 - w) / 2)
        self.set_draw_color(190, 190, 190)
        self.set_fill_color(125, 125, 0)
        self.set_text_color(225, 255, 255)
        self.set_line_width(0.6)
        self.cell(w, 9, title, 1, 1, 'C', 1)
        self.ln(10)

    def footer(self):
        self.set_y(-10)
        self.set_font('Arial', 'I', 6)
        self.cell(0, 8, 'Page '+str(self.page_no())+'/{nb}', 0, 0, 'C')

    def chapter_title(self, line):
        self.set_font('Arial', 'B', 12)
        self.set_fill_color(125, 190, 0)
        self.cell(0, 15, line, 0, 2)

    def chapter_body(self, line):
        self.set_font('Times', '', 10)
        self.multi_cell(0, 12, line, 0, 'L')


with conn:
    pdf = PDF()
    pdf.add_page()
    pdf.alias_nb_pages()
    query = "SELECT * FROM tweets"
    cur.execute(query)
    rows = cur.fetchall()
    desc = cur.description
    desc = desc[1][0]+"s | "+desc[2][0]
    pdf.chapter_title(desc)
    line = ''
    for row in rows:
        tweet = row["Tweet"].replace("\n", ". ").replace(
            "\n\n", ". ").replace("\u2026", "...")
        tweet_date = datetime.strftime(datetime.strptime(
            str(row["Date"]), '%Y-%m-%d %H:%M:%S'), '%H:%M, %b %d %Y')
        line += tweet+"\n"+tweet_date+"\n\n"
        print(line)
    pdf.chapter_body(line)
    pdf.output('twitterdb2pdf.pdf', 'F')
