import base64
import requests
import json
from datetime import datetime, timedelta
import pymysql

conn = pymysql.connect(
    host='localhost',
    user='root',
    password='root',
    db='twitterdb',
)
cur = conn.cursor()

client_key = 'B0n6807P0aOaH2fUeRrRAbaav'
client_secret = 'QHnuXFV6S5c6OSbSyPI5KzOtMSHeTdKdGI8HrcFYfcjX9nW262'

key_secret = '{}:{}'.format(client_key, client_secret).encode('ascii')
b64_encoded_key = base64.b64encode(key_secret)
b64_encoded_key = b64_encoded_key.decode('ascii')

base_url = 'https://api.twitter.com/'
auth_url = '{}oauth2/token'.format(base_url)

auth_headers = {
    'Authorization': 'Basic {}'.format(b64_encoded_key),
    'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
}

auth_data = {
    'grant_type': 'client_credentials'
}

auth_resp = requests.post(auth_url, headers=auth_headers, data=auth_data)

access_token = auth_resp.json()["access_token"]

search_headers = {
    'Authorization': 'Bearer {}'.format(access_token)
}

search_params = {
    'q': 'avengers',
    'result_type': 'recent',
    'count': 2,
    'include_entities': False,
    # 'tweet_mode': 'extended'
}

search_url = '{}1.1/search/tweets.json'.format(base_url)

search_resp = requests.get(
    search_url, headers=search_headers, params=search_params)

tweet_data = search_resp.json()
tweets = []
# print(json.dumps(tweet_data['statuses'], sort_keys=True, indent=4))
with conn:
    for x in tweet_data['statuses']:
        if 'retweeted_status' in x:
            if 'full_text' in x['retweeted_status']:
                tweet = x['retweeted_status']['full_text']
            elif 'text' in x['retweeted_status']:
                tweet = x['retweeted_status']['text']

            UTC_created_at = x['retweeted_status']['created_at'].split()

        created_time = ''
        for i in range(len(UTC_created_at)):
            if i != 0 and i != 4:
                created_time += UTC_created_at[i] + ' '
        created_time = datetime.strptime(
            created_time, '%b %d %H:%M:%S %Y ')
        IST_created_time = datetime.strftime(
            created_time + timedelta(hours=5, minutes=30), '%Y-%m-%d %H:%M:%S')

        tweet_records = (tweet, IST_created_time)
        print(tweet_records)

        query = "INSERT INTO tweets (Tweet, Date) VALUES (%s,%s);"
        cur.execute(query, tweet_records)
